import nltk
from nltk.corpus import brown
from nltk.corpus import PlaintextCorpusReader
from nltk import ngrams


def tagger(text_to_parse_sents, vulgarity_set):
    sents_with_vulgs = []
    for i in text_to_parse_sents:
        temp_sent = set(i)
        if len(vulgarity_set.intersection(temp_sent)) > 0:
            sents_with_vulgs.append(i)
     
    tagged_vulgs = []   
    for i in sents_with_vulgs:
        joined = " ".join(i)
        tagged_vulgs.append(nltk.tag.pos_tag(nltk.tokenize.word_tokenize(joined)))

    tagged_with_vulg_tag = []    
    for i in tagged_vulgs:
        tagged_sent = []
        for j in i:
            if(j[0] in vulgarity_set):
                j = (j[0], 'VULG')
            tagged_sent.append(j)
        tagged_with_vulg_tag.append(tagged_sent)

    return tagged_with_vulg_tag


def the_brown_cleanse():

    brown_sents = brown.sents(categories = 'humor')

    fivegrams = []
    for i in brown_sents:
        joined = " ".join(i)
        fg = ngrams(joined.split(), 5)
        fivegrams.append(list(fg))


    the_brown_cleanser = {}
    tagged_grams = []
    for f in fivegrams:
        for s in f:
            joined = " ".join(s)
            tagged_grams.append(nltk.tag.pos_tag(nltk.tokenize.word_tokenize(joined)))

    for j in tagged_grams:
        struct_map = ""
        for i in range(5):
            if(i != 2):
                struct_map += j[i][1]
            else:
                struct_map += '*'
        the_brown_cleanser[struct_map] = j

    return the_brown_cleanser

def clean_the_mean(tagged_with_vulg_tag, the_brown_cleanser):
    for i in tagged_with_vulg_tag:
        for j in range(len(i)):
            if i[j][1] == 'VULG':
                try:
                    vulgarity_index = j
                    struct = i[j-2][1] + i[j-1][1] + "*" + i[j+1][1] + i[j+2][1]

                    replacer = the_brown_cleanser[struct]
                    resent = []
                    for z in replacer:
                        resent.append(z[0])
                    print(" ".join(resent))
                    print("SENTENCE WITH REPLACED WORDS") 
                    fullsent = []
                    for k in range(len(i)):
                        if((k + 2) == j):
                            fullsent.append(replacer[0][0])
                        elif(( k + 1 )== j):
                            fullsent.append(replacer[1][0])
                        elif(k == j):
                            fullsent.append(replacer[2][0])
                        elif(( k - 1 )== j):
                            fullsent.append(replacer[3][0])
                        elif(( k - 2 )== j):
                            fullsent.append(replacer[4][0])
                        else:
                            fullsent.append(i[k][0])
                    print(" ".join(fullsent))
                except:
                    #print("ioob or key error")
                    x = 1
def main():
    vulgarity = PlaintextCorpusReader('.', 'words.txt')
    text_to_parse = PlaintextCorpusReader('.', 'superthread.txt')

    vulgarity_words = vulgarity.words()
    text_to_parse_sents = text_to_parse.sents()


    vulgarity_set = set(vulgarity_words)
    vulgarity_set.remove("-")
    vulgarity_set.remove("s")
    vulgarity_set.remove("'")

    tagged_with_vulg_tag = tagger(text_to_parse_sents, vulgarity_set)
    brown_cleanse = the_brown_cleanse()
    clean_the_mean(tagged_with_vulg_tag, brown_cleanse)

main()
