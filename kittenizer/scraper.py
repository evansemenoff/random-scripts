from bs4 import BeautifulSoup as bs
import requests


def scrape_thread(board, thread):

    url = "https://a.4cdn.org/" + board + \
          "/thread/" + thread + ".json"
    data = requests.get(url).json()

    posts = []
    for i in data['posts']:
        if "com" in i:
            no =   bs(str(i["no"]), features="html.parser").get_text()
            com = i["com"]
            com = com.replace("<br>", "\n")
            com =  bs(com, features="html.parser").get_text()
            post = no + "\n" + com + "\n" 
            posts.append(post)

    return posts


def get_threads(board):

    threads = []
    url = "https://a.4cdn.org/" + board + \
          "/threads.json"

    data = requests.get(url).json()

    for pages in data:
        for thread in pages['threads']:
            threads.append(thread['no'])

    return threads

    
def main():

    inpo = input('enter boards:' )
    boards = inpo.split()

    f = open(inpo + ".txt", 'a+')
    for board in boards:
        threads = get_threads(board)
        for thread in threads:
            for post in scrape_thread(board, str(thread)):
                f.write(post + "\n")

    f.close()


main()




