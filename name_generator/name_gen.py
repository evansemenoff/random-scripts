from random import randint as rand

def readfile():
    """ reads in the file of all common names in the world and generates
    the list of list of chars"""
    namelist = []
    f = open("names.txt", "r")
    for name in f:
        namelist.append(list(name.rstrip()))

    return namelist


def gen_model(namelist):
    """generates a simple bigram model for character mappings"""
    name_table = {}
    for name in namelist:
        for char in range(len(name)-1):
            if name[char] not in name_table:
                name_table[name[char]] = []
            else:
                name_table[name[char]].append(name[char + 1])

    return name_table

def gen_random(name_len, name_table):
    """generates a random name of length 'name_len'"""
    s = ""
    upper_alpha = "ABCDEFGHIJKLMNOPQRSTUVQXYZ"
    char = upper_alpha[rand(0,25)]
    for i in range(name_len):
        s += char
        char = name_table[char][rand(0,len(name_table[char])-1)]

    return s


def find_mean_name(model, name_in):
    """given a name, finds how many iterations it takes on average
    for that name to be generated"""
    total = 0
    l = len(name_in)
    for i in range(100):
        z = 0
        name = ""
        while name!= name_in:
            name = gen_random(l, model)
            z += 1
        total += z

    print(total/100)


def main():

    namelist = readfile()
    name_tab = gen_model(namelist)

    name = gen_random(rand(3,10), name_tab)

    print(name)

main()
