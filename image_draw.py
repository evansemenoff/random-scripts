import pyautogui as p
from PIL import Image
import time
p.PAUSE = 0

scaling_factor = int(input("please input scaling factor: "))
filename = input("please input filename: ")

im = Image.open(filename)
im = im.resize((im.size[0]//scaling_factor, im.size[1]//scaling_factor))
width, height = im.size
print(width, height)
startx = 300
starty = 300
if im.mode != "1":
    im = im.convert("1")
time.sleep(10)
sleepbuffer = 0
for i in range(0, width):
    for j in range(0, height):
        if im.getpixel((i,j)) != 255:
            p.click(i + startx, j + starty)
            p.mouseUp()
            sleepbuffer += 1
        if sleepbuffer == 5000: # this is to prevent getting booted from too many requests
            time.sleep(5)
            sleepbuffer = 0