x = 1
p = 1
intList, primeList = [], []


# function composition fold functions
def cmp2(f, g):
    return lambda x: f(g(x))

def cmp(l):
    if len(l) == 1:
        return l[0]
    if len(l) == 2:
        return cmp2(l[0], l[1])
    else:
        newf = cmp2(l[0], l[1])
        newl = [newf] + l[2:]
        return cmp(newl)


# fns of arity 1, used to generate the i/j elements of the matrix

def UnSize(n):
    z = 0
    for i in range(1,n):
        if gcd(i, n) == 1:
            z += 1
    return z
    
        
def fib(n):
    a, b = 0, 1
    for i in range(0, n):
        a, b = b, a + b
    return a


def primes(n):
    global primeList
    if not primeList:
        for i in range (2, 10000):
            intList.append(i)

    for i in range(len(intList)):
        if len(intList) > 0:
            primeList.append(intList.pop(0))
            intList = filter(lambda x: x % primeList[i], intList)
    return primeList[n]

def max_eul(n):
    i = 0
    num = n
    while num != 0:
        i += num % 2
        num = i // 2
        
    return i * 200

def integers(n):
    return n


# fns of arity 2, can be passed into draw fn to define how the i, jth entry of the matrix is combined
# to a singular value

def bwa(a, b):
    return a ^ b

def plus(a, b):
    return a + b

def minus(a, b):
    return a - b

def times(a, b):
    return a * b

def power(a, b):
    return a ** b

def gcd(n,m):
    if m == 0 or n == 0:
        return 0
    if m > n:
        return gcd(m,n)
    if n % m == 0:
        return m
    else:
        return gcd(m, n % m)

##########################################################

def square(s, f, g):
    matrix = []
    for i in range(s):
        newRow = []
        for j in range(s):
            k = g(f(i), f(j))
            newRow.append(k)
        matrix.append(newRow)
    return matrix


def render(n, l):
    for i in range(len(l)):
        for j in range(len(l[i])):
            val = (l[i][j] % n)
            #fill(255*cos(val)+128,255*cos(val+PI*4/3)+128,255*cos(val+PI*2/3)+128)
            fill(val%255,val%255,val%255)
            rect(i*4,j*4,4,4)


def setup():
    global func
    size(1000,1000)
    noStroke()
    flist = [UnSize]
    compfunc = cmp(flist)
    func = square(250, compfunc, gcd)
    
    
def draw():
    global x, func
    render(x, func)

    
def keyPressed():
    global x, p
    if key == "=":
        x += 1
        print(x)
    if key == "-":
        x -= 1
        print(x)
    