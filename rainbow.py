waveX, waveY, waveXV, waveYV, waveE = [], [], [], [], []
waveTotal, f, b, osil, osil2, osil3 = 0,0, 255, 0, 0, 0
base_size, z= 50, 0
xpos, q = -1100, 32
def setup():
    size(1000,1000, P3D)
    noStroke()
    frameRate(300)
    strokeWeight(10)
    for i in range(q):
        waveGenerator()


def draw():
    global waveX, waveY, waveV, waveTotal, osil, osil2, xpos, osil3, z
    global truthList
    # fill(-b*cos(osil)+128,
    #     -b*cos(osil+PI*4/3)+128,
    #     -b*cos(osil+PI*2/3)+128,
    #     3)    
    #rect(0,0, 1000, 1000)
    translate(500,500)
    scale(1.5)
    rotate(osil)
    waveMover()

    osil += PI/756
    if osil > 2*PI:
        osil = 0

def waveMover():
    global waveX, waveY, waveXV, waveYV, waveTotal, waveE, b, osil
    
    for i in range(waveTotal):
        
        waveX[i] = waveX[i] + waveXV[i]
        waveY[i] = waveY[i] + waveYV[i]
        stroke(b*cos(osil)+128,
               b*cos(osil+PI*4/3)+128,
               b*cos(osil+PI*2/3)+128,
               7)
        line(waveX[i], waveY[i], waveX[i-1], waveY[i-1])

        if(waveX[i] < -500) or (waveX[i] > 500):
            waveXV[i] = waveXV[i] * -1
            
        if (waveY[i] < -500) or (waveY[i] > 500):
            waveYV[i] = waveYV[i] * -1

def waveGenerator():
    global waveX, waveY, waveXV, waveYV, waveTotal, waveE, f
    waveX.append(0)
    waveY.append(0)
    
    waveXV.append(cos(f))
    waveYV.append(sin(f))
    waveTotal += 1
    f += TAU/q
    
    