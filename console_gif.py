import operator
import argparse
from PIL import Image
from time import sleep


def gif2txt(filename, reverse=False):
    maxLen = 80

    chs = " .:-=+*#%@"

    if reverse:
        chs = chs[::-1]
    try:
        img = Image.open(filename)
    except IOError:
        exit("file not found: {}".format(filename))

    width, height = img.size
    rate = float(maxLen) / max(width, height)
    width = int(rate * width)
    height = int(rate * height)

    palette = img.getpalette()
    strings = []

    try:
        while 1:
            img.putpalette(palette)
            im = Image.new('RGB', img.size)
            im.paste(img)
            im = im.resize((width, height))
            string = ''
            for h in range(height):
                for w in range(width):
                    rgb = im.getpixel((w, h))
                    string += chs[int(sum(rgb) / 3.0 / 256.0 * len(chs))]
                string += '\n'
            if isinstance(string, bytes):
                string = string.decode('utf8')
            strings.append(string)
            img.seek(img.tell() + 1)
            print(string)
            sleep(.1)
    except EOFError:
        pass


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('filename',
                        help='Gif input file')
    parser.add_argument('-r', '--reverse', action='store_true',
                        default=False,
                        help='Reverse')

    args = parser.parse_args()

    gif2txt(
        filename=args.filename,
        reverse=args.reverse
    )

if __name__ == '__main__':
    main()